import React, {useEffect} from "react";
import { useSelector, useDispatch } from "react-redux";
import styled from "styled-components";
import { QuestionOutlined } from '@ant-design/icons';
import { Image } from 'antd';

function Container() {
    const dispatch = useDispatch();
    const duplicateImages= useSelector((state) => state.image.duplicateImages);
    const numberOfPairs = useSelector((state) => state.image.numberOfPairs);
    const start = useSelector((state) => state.image.start);
    const turnedBack = useSelector((state) => state.image.turnedBack);
    const nubmerOfFoundPairs = useSelector((state) => state.image.nubmerOfFoundPairs);
    var score=10/numberOfPairs*nubmerOfFoundPairs;

    useEffect(()=>{
        dispatch({type:"SHUFFLE"});
        dispatch({type:"DUPLICATE_CARDS"});
        setTimeout(() => {
            dispatch({type:"START", value: true});

        }, 5000);
    }, []);
    useEffect(()=>{
        setTimeout(() => {
            if(turnedBack){
                dispatch({type:"TURNED_BACK"});
            }

        }, 1000);
    }, [turnedBack]);

    var span=numberOfPairs<=10 ?4:6;
    var height=numberOfPairs*2/span
console.log(height,100/height)
   const cols= duplicateImages.map((img, index)=>{
       return(
            !start || (img.isActive && start)?
                   <ImageContainer width={100/span} height={100/height} key={img.src + index} >
                       <DivContainer>
                           <img  data-index={img.id} src={img.src}/>

                       </DivContainer>

                   </ImageContainer>
           :
                (img.isValidate ?
                            <ImageContainer
                                          width={100/span}
                                          height={100/height-2*height}
                                          key={"div" + index}
                                          onClick={() => {
                                              dispatch({type:"ACTIVE_IMAGE", id:img.id});

                                          }}>
                                <DivContainer color="transparent">

                                </DivContainer>

                            </ImageContainer>
                        :
                            <ImageContainer
                                          width={100/span}
                                          height={100/height}
                                          key={"div" + index}
                                          onClick={() => {
                                              dispatch({type:"ACTIVE_IMAGE", id:img.id});

                                          }}>
                                <DivContainer color="#1890FF">
                                    <QuestionOutlined  style={{color:"#fff", fontSize:"24px"}}/>

                                </DivContainer>

                            </ImageContainer>
                )


       )
   })

    return (
        <GridContainer>
            {score<10 ? <> {cols}</>
            :
               <ImageContainer width={100} height={100}> Success, You find All pairs !</ImageContainer>

            }
        </GridContainer>
    );
}

export default Container;
const GridContainer = styled.div`
  width: 50%;
  height: 90%;
  margin: 0;
  text-align: center;
  display: flex;
  flex-wrap: wrap;
  overflow:hidden;
`;

const ImageContainer = styled.div`
  display: flex;
  width: ${(props) => props.width + "%"};
  height: ${(props) => props.height + "%"};
  justify-content:center;
  overflow: auto;
  flex-wrap: wrap;
  align-items: center;
  border-radius: 4px;
  overflow: hidden;
  position: relative;
  cursor: pointer;
  padding:2px;
`

const DivContainer = styled.div`
        display: flex;
    width: 100%;
    height: 100%;
    border-radius: 4px;
    justify-content: center;
    align-items: center;
    background-color: ${(props) => props.color};
   img {
    width:100%;
    height: 100%;
    transform: scaleY(-1);
    border-radius: 4px;
  }
`