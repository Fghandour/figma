import styled from "styled-components";
import { Card,Menu, Button, Dropdown, Divider } from "antd";
import { useSelector, useDispatch } from "react-redux";
import { DownOutlined } from '@ant-design/icons';

function Control() {
    const dispatch = useDispatch();
    const numberOfPairs = useSelector((state) => state.image.numberOfPairs);
    const numberOfAttempts = useSelector((state) => state.image.numberOfAttempts);
    const nubmerOfFoundPairs = useSelector((state) => state.image.nubmerOfFoundPairs);
    var score=10/numberOfPairs*nubmerOfFoundPairs;
    const menu = () => {
        return (
            <Menu>
                <Menu.Item
                    onClick={() => {
                        dispatch({type:"CHANGE_PAIRS", number:6});
                        dispatch({type:"SHUFFLE"});
                        dispatch({type:"DUPLICATE_CARDS"});
                        dispatch({type:"START", value: false});
                        setTimeout(() => {
                            dispatch({type:"START", value: true});

                        }, 5000);

                    }}
                >
                    6 pairs
                </Menu.Item>
                <Menu.Item
                    onClick={() => {
                            dispatch({type:"CHANGE_PAIRS", number:8});
                        dispatch({type:"SHUFFLE"});
                        dispatch({type:"DUPLICATE_CARDS"});
                        dispatch({type:"START", value: false});
                        setTimeout(() => {
                            dispatch({type:"START", value: true});

                        }, 5000);

                    }}
                >
                    8 pairs
                </Menu.Item>
                <Menu.Item
                    onClick={() => {
                            dispatch({type:"CHANGE_PAIRS", number:10});
                        dispatch({type:"SHUFFLE"});
                        dispatch({type:"DUPLICATE_CARDS"});
                        dispatch({type:"START", value: false});
                        setTimeout(() => {
                            dispatch({type:"START", value: true});

                        }, 5000);
                    }}
                >
                    10 pairs
                </Menu.Item>
                <Menu.Item
                    onClick={() => {
                            dispatch({type:"CHANGE_PAIRS", number:12});
                        dispatch({type:"SHUFFLE"});
                        dispatch({type:"DUPLICATE_CARDS"});
                        dispatch({type:"START", value: false});
                        setTimeout(() => {
                            dispatch({type:"START", value: true});

                        }, 5000);
                    }}
                >
                    12 pairs
                </Menu.Item>
                <Menu.Item
                    onClick={() => {
                            dispatch({type:"CHANGE_PAIRS", number:15});
                        dispatch({type:"SHUFFLE"});
                        dispatch({type:"DUPLICATE_CARDS"});
                        dispatch({type:"START", value: false});
                        setTimeout(() => {
                            dispatch({type:"START", value: true});

                        }, 5000);

                    }}
                >
                    15 pairs
                </Menu.Item>
                <Menu.Item
                    onClick={() => {
                            dispatch({type:"CHANGE_PAIRS", number:18});
                        dispatch({type:"SHUFFLE"});
                        dispatch({type:"DUPLICATE_CARDS"});
                        dispatch({type:"START", value: false});
                        setTimeout(() => {
                            dispatch({type:"START", value: true});

                        }, 5000);

                    }}
                >
                    18 pairs
                </Menu.Item>
                <Menu.Item
                    onClick={() => {
                            dispatch({type:"CHANGE_PAIRS", number:21});
                        dispatch({type:"SHUFFLE"});
                        dispatch({type:"DUPLICATE_CARDS"});
                        dispatch({type:"START", value: false});
                        setTimeout(() => {
                            dispatch({type:"START", value: true});

                        }, 5000);
                    }}
                >
                    21 pairs
                </Menu.Item>
            </Menu>
        );
    };

    return (
        <ControlContainer>

                <ScoreContainer>
                    <TitleStyle>Score</TitleStyle>

                    <div style={{margin:"5px"}}>
                        <TitleStyle color={"blue"} >{Math.round(score) }</TitleStyle>
                        <TitleStyle>/ 10 </TitleStyle>
                        </div>
                    <span style={{margin:"5px"}}> Tries: {numberOfAttempts}</span>

                </ScoreContainer>
            <Divider />

            <ScoreContainer >
                <TitleStyle>Options</TitleStyle>

                <MenuContainer>
                    <span>Size: </span>
                    <Dropdown overlay={menu()} placement="bottomCenter">
                        <Button>{numberOfPairs} pairs <DownOutlined /> </Button>
                    </Dropdown>
                </MenuContainer>

            </ScoreContainer>
  <ScoreContainer>
            <Button
                type="primary"
                style={{ display: "block", margin: "20px auto 10px 10px" }}
                onClick={() => {
                    dispatch({type:"SHUFFLE"});
                    dispatch({type:"DUPLICATE_CARDS"});
                    dispatch({type:"START", value: false});
                    setTimeout(() => {
                        dispatch({type:"START", value: true});

                    }, 5000);
                }}
            >
                Restart
            </Button>
  </ScoreContainer>

        </ControlContainer>


    );
}

export default Control

const ControlContainer = styled.div`
  position: relative;
  background-color:#ffffff;
  height: 60%;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  width:20%;
  margin:0;
}
`;
const MenuContainer = styled.div`
  position: relative;
  display:flex;
  justify-content:space-around;
  width: 100%;
`;
const ScoreContainer = styled.div`
     position: relative;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    margin: 0;
    width: 100%;
`;
const TitleStyle=styled.span`
     font-size: 18px;
    font-family: sans-serif;
    font-weight: bold;
     margin: 5px;
    color: ${(props) => props.color};
    `