import '../styles/globals.css'
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";

import rootReducer from "../redux/index";
const middleware = applyMiddleware(thunk, logger);
const store = createStore(rootReducer, middleware);



function MyApp({ Component, pageProps }) {
  return(
      <Provider store={store}>
        <Component {...pageProps} />  </Provider>
  )

}

export default MyApp
