import { Layout } from 'antd';
import Control from "../components/Control"
import Container from "../components/Container"
const { Header, Content } = Layout;

import styled from "styled-components";

function App() {

  return (
      <Layout style={{ height: "100%",width: "100%", margin: "0 auto", display: "flex", backgroundColor:"#EFF0F1" }}>
         <Header style={{ width: "100%", justifyContent: "center", display: "flex", backgroundColor:"#EFF0F1" }}>
           <HeaderStyle>Find The pair</HeaderStyle>
         </Header>
          <Content>
              <ContentStyle>
                      <Container/>

                       <Control/>
              </ContentStyle>

          </Content>
      </Layout>
  );
}

export default App
const HeaderStyle = styled.div`
  position: relative;
     font-size: 24px;
    font-family: sans-serif;
    font-weight: bold;
    justify-content: center;
`;
const ContentStyle = styled.div`
   position: relative;
   display:flex;
   justify-content: space-around;
   align-item:center;
   height: 100%;
`;
