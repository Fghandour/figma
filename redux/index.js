import { combineReducers, createStore } from "redux";
import imageReducer from "./reducers/index";
const rootReducer = combineReducers({
    image: imageReducer,
});

export default rootReducer;
