export const FLIP_CARD = "FLIP_CARD";
export const CHANGE_PAIRS = "CHANGE_PAIRS";
export const SHUFFLE = "SHUFFLE";
export const DUPLICATE_CARDS = "DUPLICATE_CARDS";
export const START = "START";
export const ACTIVE_IMAGE = "ACTIVE_IMAGE";
export const TURNED_BACK = "TURNED_BACK";
