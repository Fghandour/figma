import { FLIP_CARD, CHANGE_PAIRS,SHUFFLE,DUPLICATE_CARDS,START,ACTIVE_IMAGE, TURNED_BACK} from "../constants/actionTypes";
const clone = require("rfdc")();

const initState = {
    numberOfPairs:6,
    images: [
        {
            src: "/images/pair-1.jpg",
            isActive: false,
        },
        {
            src: "/images/pair-2.jpg",
            isActive: false,
        },
        {
            src: "/images/pair-3.jpg",
            isActive: false,
        },
        {
            src: "/images/pair-4.jpg",
            isActive: false,
        },
        {
            src: "/images/pair-5.jpg",
            isActive: false,
        },
        {
            src: "/images/pair-6.jpg",
            isActive: false,
        },
        {
            src: "/images/pair-7.jpg",
            isActive: false,
        },
        {
            src: "/images/pair-8.jpg",
            isActive: false,
        },
        {
            src: "/images/pair-9.jpg",
            isActive: false,
        },
        {
            src: "/images/pair-10.jpg",
            isActive: false,
        },
        {
            src: "/images/pair-11.jpg",
            isActive: false,
        },
        {
            src: "/images/pair-12.jpg",
            isActive: false,
        },
        {
            src: "/images/pair-13.jpg",
            isActive: false,
        },
        {
            src: "/images/pair-14.jpg",
            isActive: false,
        },
        {
            src: "/images/pair-15.jpg",
            isActive: false,
        },
        {
            src: "/images/pair-16.jpg",
            isActive: false,
        },
        {
            src: "/images/pair-17.jpg",
            isActive: false,
        },
        {
            src: "/images/pair-18.jpg",
            isActive: false,
        },
        {
            src: "/images/pair-19.jpg",
            isActive: false,
        },
        {
            src: "/images/pair-20.jpg",
            isActive: false,
        },
        {
            src: "/images/pair-21.jpg",
            isActive: false,
        },
    ],
    duplicateImages:[],
    revealImg:0,
    start:false,
    flippedImg:[],
    score:0,
    numberOfAttempts:0,
    nubmerOfFoundPairs:0,
    turnedBack:false
};
const imageReducer = (state = initState, action) => {
    switch (action.type) {
        case FLIP_CARD:
            return {
                ...state,
                data: action.data,
            };
        case TURNED_BACK:
            var duplicateImages=state.duplicateImages
            duplicateImages.forEach((el)=>{
                let flippedCardIndex = state.flippedImg.findIndex(
                    (flImage) => el.id === flImage.id
                );
                if(flippedCardIndex>-1){
                    el.isActive=false;
                }
            })
            return {
                ...state,
                duplicateImages:[...duplicateImages],
                flippedImg:[],
                turnedBack: false,
            };
        case CHANGE_PAIRS:
            return {
                ...state,
                numberOfPairs:action.number,
                score:0,
                revealImg:0,
                flippedImg:[],
                numberOfAttempts:0,
                nubmerOfFoundPairs:0,

            };
        case SHUFFLE:
            let array = shuffle(state.images);

            return {
                ...state,
                images:[...array]
            };
        case DUPLICATE_CARDS:
            let duplicatedImages = clone(state.images).splice(0,state.numberOfPairs);
            var duplicatedImages1=clone(duplicatedImages)
            let newDuplicatedImages =shuffle([...duplicatedImages, ...duplicatedImages1]);
            newDuplicatedImages.forEach((img, index)=>{
                    img.id=index;
                    img.isValidate=false
            })

            return {
                ...state,
                duplicateImages:[...newDuplicatedImages]
            };
            case START:

            return {
                ...state,
                start:action.value
            };
            case ACTIVE_IMAGE:
                 array=state.duplicateImages;
                 var numberOfAttempts=state.numberOfAttempts
                var nubmerOfFoundPairs=state.nubmerOfFoundPairs
                 var revealImg=state.revealImg
                var score=state.score;
                 var flippedImg=state.flippedImg;
                 var turnedBack=state.turnedBack;
                var found= array.find(
                (el) => el.id === action.id
               );
                if(revealImg===0){
                    array.forEach((el,index) => {
                        if (el.id === action.id) {
                            el.isActive = true;
                        }
                    });
                    revealImg=revealImg+1;
                    flippedImg.push(found);

                }else if(revealImg===1){
                    if(state.flippedImg && state.flippedImg[0].src===found.src){
                        revealImg=0;
                        array.forEach((el,index) => {
                            if (el.src === state.flippedImg[0].src) {
                                el.isActive = false;
                                el.isValidate=true;
                            }
                        });
                        flippedImg=[];
                        numberOfAttempts++;
                        nubmerOfFoundPairs++;
                        score=10/state.numberOfPairs*nubmerOfFoundPairs;


                    }else{
                            revealImg=0;
                            array.forEach((el,index) => {
                                if (el.id===state.flippedImg[0].id || el.id===found.id ) {
                                    el.isActive = true;

                                }
                            });
                        flippedImg.push(found);
                        numberOfAttempts++;
                        turnedBack=true;

                    }

                }

            return {
                ...state,
                duplicateImages:[...array],
                revealImg:revealImg,
                flippedImg:[...flippedImg],
                numberOfAttempts:numberOfAttempts,
                nubmerOfFoundPairs:nubmerOfFoundPairs,
                score:score,
                turnedBack:turnedBack
            };
    }

    return state;
};
function shuffle(array){

    let len = array.length - 1;
    for (let i = len; i > 0; i--) {
        const j = Math.floor(Math.random() * i);
        const temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array
}

export default imageReducer;
